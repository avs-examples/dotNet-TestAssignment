Environment: VisualStudio Community 2017
DataBase: LocalDB

Setup:
	Run Polls.Server project!
	If you have some problems:
		1. Check NuGet packages
		2. Restore Packages for package.json in Polls.Server project
		3. Set as start up project Polls.Server
		4. Run
