﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Polls.Data;
using System.Linq;

namespace Polls.Tests
{
    [TestClass]
    public class DataTest
    {
        [TestMethod]
        public void CheckDefaultData()
        {
            IPollRepository repository = new PollRepository();

            var poll = repository.GetFirstPoll();

            Assert.IsNotNull(poll, "First poll not exist");

            Assert.IsTrue(poll.PollAnswers.Count > 0, "Missed poll answers");

            repository.Vote(poll.PollAnswers.First().Id, "Test Testovich");

            var report = repository.GetFirstReport();

            Assert.IsTrue(report.Any(x => x.Value > 0));            
        }
    }
}
