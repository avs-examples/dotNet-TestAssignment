﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;

namespace Polls.Data
{
    public interface IPollRepository
    {
        /// <summary>
        /// Load first poll from database or throw exception 
        /// </summary>
        /// <returns>First poll</returns>
        Poll GetFirstPoll();

        /// <summary>
        /// Load poll by id or throw exception 
        /// </summary>
        /// <param name="id">Poll Id</param>
        /// <returns>Poll</returns>
        Poll GetById(int id);

        /// <summary>
        /// Get vote report for first poll
        /// </summary>
        /// <returns>Poll report</returns>
        Dictionary<string, int> GetFirstReport();

        /// <summary>
        /// Get vote report for poll by id
        /// </summary>
        /// <param name="id">Poll Id</param>
        /// <returns>Poll report</returns>
        Dictionary<string, int> GetReportById(int id);

        /// <summary>
        /// Write vote for answer with id = answerId
        /// </summary>
        /// <param name="answerId">Answer ID</param>
        /// <param name="fio">FIO of voter</param>
        void Vote(int answerId, string fio);
    }

    public class PollRepository : IPollRepository
    {
        public Poll GetFirstPoll()
        {
            using(var db = new PollContext())
            {                
                var poll = db.Polls.Include(x => x.PollAnswers).FirstOrDefault();
                if (poll != null)
                    return poll;
                else
                    throw new Exception("Poll data table is empty");                
            }
        }

        public Poll GetById(int id)
        {
            using (var db = new PollContext())
            {
                var poll = db.Polls.Include(x => x.PollAnswers).SingleOrDefault(x => x.Id == id);
                if (poll != null)
                    return poll;
                else
                    throw new Exception($"Poll with id = {id} not found");
            }
        }

        public Dictionary<string, int> GetFirstReport()
        {
            using (var db = new PollContext())
            {
                var poll = db.Polls.Include(x => x.PollAnswers).FirstOrDefault();
                if (poll == null)
                    throw new Exception("Poll data table is empty");

                var report = new Dictionary<string, int>();
                poll.PollAnswers.ForEach(x => report.Add(x.Answer, x.Votes.Count));

                return report;
            }
        }
        
        public Dictionary<string, int> GetReportById(int id)
        {
            using (var db = new PollContext())
            {
                var poll = db.Polls.Include(x => x.PollAnswers).SingleOrDefault(x => x.Id == id);
                if (poll == null)
                    throw new Exception($"Poll with id = {id} not found");

                var report = new Dictionary<string, int>();
                poll.PollAnswers.ForEach(x => report.Add(x.Answer, x.Votes.Count));

                return report;
            }
        }

        public void Vote(int answerId, string fio)
        {
            if (string.IsNullOrWhiteSpace(fio))
                throw new Exception("FIO have to be not empty");

            using (var db = new PollContext())
            {

                var answer = db.PollAnswers.FirstOrDefault(x => x.Id == answerId);
                if(answer == null)
                    throw new Exception("Answer not found");

                var newVote = new Vote { PollAnswerId = answerId, FIO = fio };

                db.Votes.Add(newVote);
                db.SaveChanges();
            }
        }
    }
}
