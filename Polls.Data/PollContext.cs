﻿using log4net;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Polls.Data
{
    public class Poll
    {
        [Key]
        public int Id { get; set; }
        [Required, StringLength(255), Index(IsUnique = true)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }

        public virtual List<PollAnswer> PollAnswers { get; set; }
    }
    public class PollAnswer
    {
        [Key]
        public int Id { get; set; }
        [Required, StringLength(255)]
        public string Answer { get; set; }

        public int PollId { get; set; }
        public virtual Poll Poll { get; set; }

        public virtual List<Vote> Votes { get; set; }
    }

    public class Vote
    {
        [Key]
        public int Id { get; set; }
        [Required, StringLength(255)]
        public string FIO { get; set; }

        public int PollAnswerId { get; set; }
        public virtual PollAnswer PollAnswer { get; set; }        
    }


    public class PollContext: DbContext
    {
        private static ILog logger = LogManager.GetLogger("DataLogger");

        public IDbSet<Poll> Polls { get; set; }
        public IDbSet<PollAnswer> PollAnswers { get; set; }
        public IDbSet<Vote> Votes { get; set; }

        static PollContext()
        {
            Database.SetInitializer(new PollContextInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            Database.Log = s => logger.Info(s);
        }
    }

    public class PollContextInitializer : DropCreateDatabaseAlways<PollContext>
        //DropCreateDatabaseIfModelChanges<PollContext>
    {
        protected override void Seed(PollContext context)
        {
            var poll = new Poll { Name = "Coffee Poll", Description = "How many cups of coffee do you drink every day?" };
            context.Polls.Add(poll);
            context.SaveChanges();

            context.PollAnswers.Add(new PollAnswer { PollId = poll.Id, Answer = "1 cup" });
            context.PollAnswers.Add(new PollAnswer { PollId = poll.Id, Answer = "2 cups" });
            context.PollAnswers.Add(new PollAnswer { PollId = poll.Id, Answer = "less than 5 cups" });
            context.PollAnswers.Add(new PollAnswer { PollId = poll.Id, Answer = "more than 5 cups" });
            context.SaveChanges();
        }
    }
}
