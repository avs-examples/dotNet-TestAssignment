﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using log4net;
using Microsoft.Practices.Unity;
using System.Text;

namespace Polls.Server
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            UnityConfig.GetLogger().Info("Application start");
        }

        void Application_End(object sender, EventArgs e)
        {
            UnityConfig.GetLogger().Info("Application end");
        }

        void Application_Error(object sender, EventArgs e)
        {
            HttpContext context = ((HttpApplication)sender).Context;
            Exception ex = context.Server.GetLastError();
            if (ex == null || !(ex is HttpException) || (ex as HttpException).GetHttpCode() == 404)
            {
                return;
            }
            UnityConfig.GetLogger().Error($"Error ({context.Request.Url}): ",ex);
            context.Response.StatusCode = 500;
        }

    }
}