﻿using log4net;
using Microsoft.Practices.Unity;
using Polls.Data;
using Polls.Server.Model;
using System.Linq;
using System.Web.Http;

namespace Polls.Server.Controllers
{
    public class ReportController : ApiController
    {
        [Dependency]
        public ILog Log { get; set; }

        [Dependency]
        public IPollRepository PollRepository { get; set; }

        /// <summary>
        /// Get report for first poll
        /// </summary>
        /// <returns>Poll report</returns>
        public ReportModel Get()
        {
            Log.Info("APIv1 Get report for first poll");

            var poll = PollRepository.GetFirstPoll();
            var report = PollRepository.GetFirstReport();

            var reportModel = new ReportModel
            {
                PollName = poll.Name,
                PollDescription = poll.Description,
                ReportAnswers = report.Select(x => new ReportAnswer { Answer = x.Key, Count = x.Value }).ToArray()
            };

            return reportModel;
        }

        /// <summary>
        /// Get report for poll by Id
        /// </summary>
        /// <param name="id">Poll Id</param>
        /// <returns>Poll report</returns>
        public ReportModel Get(int id)
        {
            Log.Info($"APIv1 Get report for poll by Id = {id}");
            var poll = PollRepository.GetById(id);
            var report = PollRepository.GetReportById(id);

            var reportModel = new ReportModel
            {
                PollName = poll.Name,
                PollDescription = poll.Description,
                ReportAnswers = report.Select(x => new ReportAnswer { Answer = x.Key, Count = x.Value }).ToArray()
            };

            return reportModel;

        }
    }
}
