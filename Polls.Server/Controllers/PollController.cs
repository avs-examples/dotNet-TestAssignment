﻿using log4net;
using Microsoft.Practices.Unity;
using Polls.Data;
using Polls.Server.Model;
using System.Web.Http;

namespace Polls.Server.Controllers
{
    public class PollController : ApiController
    {
        [Dependency]
        public ILog Log { get; set; }

        [Dependency]
        public IPollRepository PollRepository { get; set; }

        /// <summary>
        /// Get first poll
        /// </summary>
        /// <returns></returns>
        public PollModel Get()
        {
            Log.Info("APIv1 Get first poll");
            var poll = PollRepository.GetFirstPoll();
            var model = new PollModel(poll);
            return model;
        }

        /// <summary>
        /// Get poll by Id
        /// </summary>
        /// <param name="id">Poll id</param>
        /// <returns></returns>
        public PollModel Get(int id)
        {
            Log.Info($"APIv1 Get poll by Id = {id}");
            var poll = PollRepository.GetById(id);
            var model = new PollModel(poll);
            return model;
        }


    }
}
