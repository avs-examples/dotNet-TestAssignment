﻿using log4net;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Polls.Server.Views
{
    public class IndexController : Controller
    {
        [Dependency]
        public ILog Log { get; set; }

        public ActionResult Index()
        {            
            Log.Info("Redirect to Angular SPA");
            return new RedirectResult(url: "/app/index/index.html", permanent: false);
        }

    }
}