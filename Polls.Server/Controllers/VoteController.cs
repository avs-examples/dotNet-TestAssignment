﻿using log4net;
using Microsoft.Practices.Unity;
using Polls.Data;
using Polls.Server.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Polls.Server.Controllers
{
    public class VoteController : ApiController
    {
        [Dependency]
        public ILog Log { get; set; }

        [Dependency]
        public IPollRepository PollRepository { get; set; }

        /// <summary>
        /// Handler vote
        /// </summary>
        /// <param name="answerId">Answer Id</param>
        /// <param name="fio">FIO of voter</param>
        public void Post([FromBody]VoteModel vote)
        {
            Log.Info($"APIv1 Post vote (AnswerId: {vote.AnswerId}, FIO: {vote.FIO}");
            PollRepository.Vote(vote.AnswerId, vote.FIO);
        }
    }
}
