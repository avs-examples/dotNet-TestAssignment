using log4net;
using Microsoft.Practices.Unity;
using Polls.Data;
using System;
using System.Web.Http;

namespace Polls.Server
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);

            // Set up Unity for WebApi
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }

        public static ILog GetLogger()
        {
            var container = UnityConfig.GetConfiguredContainer();
            var log = container.Resolve<ILog>();
            return log;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            container
                .RegisterType<IPollRepository, PollRepository>()
                .RegisterInstance<ILog>(LogManager.GetLogger("ServerLogger"));
        }
    }
}