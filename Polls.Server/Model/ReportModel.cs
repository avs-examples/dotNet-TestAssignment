﻿
namespace Polls.Server.Model
{
    public class ReportModel
    {
        public string PollName { get; set; }
        public string PollDescription { get; set; }

        public ReportAnswer[] ReportAnswers { get; set; }
    }

    public class ReportAnswer
    {
        public string Answer { get; set; }
        public int Count { get; set; }
    }
}