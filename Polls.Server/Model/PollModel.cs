﻿using Polls.Data;

namespace Polls.Server.Model
{
    public class PollModel
    {
        public PollModel(Poll poll)
        {
            Id = poll.Id;
            Name = poll.Name;
            Description = poll.Description;

            var answerCount = poll.PollAnswers.Count;
            if (answerCount > 0)
            {
                PollAnswers = new PollAnswerModel[answerCount];
                for(int i = 0; i< answerCount; i++)
                    PollAnswers[i] = new PollAnswerModel(poll.PollAnswers[i]);
            }            
        }

        public int Id { get; set; }        
        public string Name { get; set; }        
        public string Description { get; set; }

        public PollAnswerModel[] PollAnswers { get; set; }
    }

    public class PollAnswerModel
    {
        public PollAnswerModel(PollAnswer answer)
        {
            Id = answer.Id;
            Answer = answer.Answer;            
        }

        public int Id { get; set; }
        public string Answer { get; set; }
    }

}