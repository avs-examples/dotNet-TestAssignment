﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PollComponent } from './component/poll.component';
import { ReportComponent } from './component/report.component';

const appRoutes: Routes = [    
    { path: 'app/index', redirectTo: 'poll', pathMatch: 'full' },
    { path: 'poll', component: PollComponent },
    { path: 'report', component: ReportComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);