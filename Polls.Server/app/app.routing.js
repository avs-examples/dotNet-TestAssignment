"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var poll_component_1 = require("./component/poll.component");
var report_component_1 = require("./component/report.component");
var appRoutes = [
    { path: 'app/index', redirectTo: 'poll', pathMatch: 'full' },
    { path: 'poll', component: poll_component_1.PollComponent },
    { path: 'report', component: report_component_1.ReportComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map