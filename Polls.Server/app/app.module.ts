﻿import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { routing } from './app.routing';

import { AppComponent } from './component/app.component';
import { PollComponent } from './component/poll.component';
import { ReportComponent } from './component/report.component';

import { ApiService } from './service/api.service';
 
@NgModule({
    imports: [BrowserModule, ReactiveFormsModule, HttpModule, routing],
    declarations: [AppComponent, PollComponent, ReportComponent],
    providers: [{ provide: APP_BASE_HREF, useValue: '/' }, ApiService],
    bootstrap: [AppComponent]
})

export class AppModule { }