﻿import { Component, OnInit } from "@angular/core";

import { IPoll, IPollAnswer } from '../model/poll';
import { IVote } from '../model/vote';

import { ApiService } from '../service/api.service';

import { Global } from '../global';

@Component({
    templateUrl: 'app/component/poll.component.html'
})

export class PollComponent implements OnInit {
    isDataLoading: boolean = false;    
    msg: string;

    poll: IPoll;

    AnswerId: number;
    VoterFIO: string;

    isVoted: boolean = false;
    formValid: boolean = false;    

    constructor(private _apiService: ApiService) { }

    ngOnInit() {
           this.LoadPoll();    
    }

    LoadPoll() {
        this.isDataLoading = true;
        this._apiService.get(Global.BASE_ENDPOINT + 'poll')
            .subscribe(poll => { this.poll = poll; this.isDataLoading = false; },
            error => this.msg = <any>error);
    }

    onFIOKeyUp(value: string) {        
        this.VoterFIO = value;
        this.formValidate();
    }

    setAnswerId(answerId: number) {
        this.AnswerId = answerId;
        this.formValidate();
    }

    formValidate() {
        this.formValid = this.AnswerId != null && this.VoterFIO != "" && this.VoterFIO != null;
    }

    onVote() {
        if(this.formValid){
            this.formValid = false;
            this.msg = "Vote sending...";

            let vote: IVote = { AnswerId: this.AnswerId, FIO: this.VoterFIO };

            this._apiService.post(Global.BASE_ENDPOINT + 'vote', vote)
                .subscribe(poll => {
                    this.msg = "Thank you for vote!";
                    this.isVoted = true;
                },
                error => {
                    this.msg = <any>error;
                    this.formValidate();
                });
        }
        
    }
    
}