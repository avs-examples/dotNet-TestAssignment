"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("../service/api.service");
var global_1 = require("../global");
var ReportComponent = (function () {
    function ReportComponent(_apiService) {
        this._apiService = _apiService;
        this.isDataLoading = false;
    }
    ReportComponent.prototype.ngOnInit = function () {
        this.LoadReport();
    };
    ReportComponent.prototype.LoadReport = function () {
        var _this = this;
        this.isDataLoading = true;
        this._apiService.get(global_1.Global.BASE_ENDPOINT + 'report')
            .subscribe(function (report) { _this.report = report; _this.isDataLoading = false; }, function (error) { return _this.msg = error; });
    };
    return ReportComponent;
}());
ReportComponent = __decorate([
    core_1.Component({
        templateUrl: 'app/component/report.component.html'
    }),
    __metadata("design:paramtypes", [api_service_1.ApiService])
], ReportComponent);
exports.ReportComponent = ReportComponent;
//# sourceMappingURL=report.component.js.map