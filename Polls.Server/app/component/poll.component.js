"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("../service/api.service");
var global_1 = require("../global");
var PollComponent = (function () {
    function PollComponent(_apiService) {
        this._apiService = _apiService;
        this.isDataLoading = false;
        this.isVoted = false;
        this.formValid = false;
    }
    PollComponent.prototype.ngOnInit = function () {
        this.LoadPoll();
    };
    PollComponent.prototype.LoadPoll = function () {
        var _this = this;
        this.isDataLoading = true;
        this._apiService.get(global_1.Global.BASE_ENDPOINT + 'poll')
            .subscribe(function (poll) { _this.poll = poll; _this.isDataLoading = false; }, function (error) { return _this.msg = error; });
    };
    PollComponent.prototype.onFIOKeyUp = function (value) {
        this.VoterFIO = value;
        this.formValidate();
    };
    PollComponent.prototype.setAnswerId = function (answerId) {
        this.AnswerId = answerId;
        this.formValidate();
    };
    PollComponent.prototype.formValidate = function () {
        this.formValid = this.AnswerId != null && this.VoterFIO != "" && this.VoterFIO != null;
    };
    PollComponent.prototype.onVote = function () {
        var _this = this;
        if (this.formValid) {
            this.formValid = false;
            this.msg = "Vote sending...";
            var vote = { AnswerId: this.AnswerId, FIO: this.VoterFIO };
            this._apiService.post(global_1.Global.BASE_ENDPOINT + 'vote', vote)
                .subscribe(function (poll) {
                _this.msg = "Thank you for vote!";
                _this.isVoted = true;
            }, function (error) {
                _this.msg = error;
                _this.formValidate();
            });
        }
    };
    return PollComponent;
}());
PollComponent = __decorate([
    core_1.Component({
        templateUrl: 'app/component/poll.component.html'
    }),
    __metadata("design:paramtypes", [api_service_1.ApiService])
], PollComponent);
exports.PollComponent = PollComponent;
//# sourceMappingURL=poll.component.js.map