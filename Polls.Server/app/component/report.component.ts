﻿import { Component, OnInit } from "@angular/core";

import { IReport } from '../model/report';

import { ApiService } from '../service/api.service';

import { Global } from '../global';

@Component({
    templateUrl: 'app/component/report.component.html'     
})

export class ReportComponent{
    isDataLoading: boolean = false;
    msg: string;

    report: IReport;    

    constructor(private _apiService: ApiService) { }

    ngOnInit(): void {
           this.LoadReport();    
    }

    LoadReport(): void {
        this.isDataLoading = true;
        this._apiService.get(Global.BASE_ENDPOINT + 'report')
            .subscribe(report => { this.report = report; this.isDataLoading = false; },
            error => this.msg = <any>error);
    }
}