﻿export interface IPoll {
    Id: number,
    Name: string,
    Description: string,

    PollAnswers: Array<IPollAnswer>
}

export interface IPollAnswer {
    Id: number,
    Answer: string
}