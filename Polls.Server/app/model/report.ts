﻿export interface IReport {    
    PollName: string,
    PollDescription: string,

    ReportAnswers: Array<IReportAnswer>
}

export interface IReportAnswer {    
    Answer: string,
    Count: number
}